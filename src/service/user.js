import axios from "axios";

export const login = async (user) => {
    const response = await axios.post( process.env.VUE_APP_API_USER+'/login', user, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
    return response
}

export const signup = async (user) => {
    const response = await axios.post(process.env.VUE_APP_API_USER+'/create', user, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    })
    return response
}
export const getAllPost = async () => {
    const response = await axios.get(process.env.VUE_APP_API_POST + "/getAll" )
    return response
}

export const findPostById = async (id)=> {
    const response = await axios.get(process.env.VUE_APP_API_POST + "/" + id)
    return response
}
