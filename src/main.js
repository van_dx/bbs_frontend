import Vue from 'vue'
import App from './App.vue';
import router from "../router";
import BootstrapVue from "bootstrap-vue";
import 'bootstrap/dist/css/bootstrap.css'

import moment from 'moment'
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm:ss')
  }
})

Vue.use(BootstrapVue)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')


