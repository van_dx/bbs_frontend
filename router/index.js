import Vue from 'vue'
import VueRouter from 'vue-router'
import DetailPost from "@/components/DetailPost";
import CreatePost from "@/components/CreatePost";
import Register from "@/components/Register";
import ListPost from "@/components/ListPost";

Vue.use(VueRouter)

const routes = [
    {
        path: '/create',
        name: 'create',
        component: CreatePost
    },
    {
        path: '/post/:id',
        name: 'detail',
        component:  DetailPost
    },
    {
        path: '/',
        name: 'home',
        component: ListPost
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})


export default router
